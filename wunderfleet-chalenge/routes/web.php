<?php

use App\Http\Controllers\AddressController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\RegistrationController;
use Illuminate\Support\Facades\Route;

Route::get('/', [RegistrationController::class, 'redirect']);

Route::prefix('customer')->group(function (){
    Route::get('/customerInfo', [CustomerController::class, 'index']);
    Route::post('/customerInfo', [CustomerController::class, 'setDataToSession']);

    Route::get('/addressInfo', [AddressController::class, 'index']);
    Route::post('/addressInfo', [AddressController::class, 'setDataToSession']);

    Route::get('/paymentInfo', [PaymentController::class, 'index']);
    Route::post('/paymentInfo', [CustomerController::class, 'store']);
});

Route::prefix('registration')->group(function(){
    Route::get('/success', [RegistrationController::class, 'success']);
    Route::get('/error', [RegistrationController::class, 'error']);
});





