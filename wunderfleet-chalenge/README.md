## Wunder Mobility Test

### Requirements
- Docker

### Description
This test was implemented using Laravel 8.0, MySQL 8.0 and MVC pattern.

#### Database Model
![Alt text](database_model.png?raw=true "Database Model")
### Application Instalation

```
# Define environment variables

export APP_PORT=80
export APP_SERVICE="wunder.test"
export DB_PORT=3306
export WWWUSER=$UID
export WWWGROUP=$(id -g)

# start the container
docker-compose up -d --build

# copy the env file from env-example
cp .env.example .env

# run migrations
docker-compose exec wunder.test php artisan migrate

# access at http://localhost:80
```
### Tests
To run unit tests use the command:
```
docker-compose exec wunder.test ./vendor/bin/phpunit
```

#### What would I do better?

I would implement more test cases, notifications and more specific exceptions
