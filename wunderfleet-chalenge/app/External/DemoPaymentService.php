<?php


namespace App\External;


use App\Models\Payment;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

class DemoPaymentService
{
    /**
     * @var string
     */
    private $baseUri;
    /**
     * @var Client
     */
    private $client;

    /**
     * DemoPaymentService constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->baseUri = env('DEMO_PAYMENT_URL');
    }

    /**
     * @param Payment $paymentData
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function getExternalPaymentData(Payment $paymentData)
    {
        $data = $this->createRequest($paymentData);
        return $this->client->post($this->baseUri, [
            'json' => $data
        ]);
    }

    /**
     * @param Payment $paymentData
     * @return array
     */
    public function createRequest(Payment $paymentData)
    {
        return [
                'customerId' => $paymentData->customer_id,
                'iban' => $paymentData->iban,
                'owner' => $paymentData->account_owner
        ];
    }

}
