<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function index(Request $request)
    {
        $request->session()->put('currentStep', 2);
        return view('registration/addressInfo');
    }

    public function setDataToSession(Request $request)
    {
        $request->session()->put('street', $request->street);
        $request->session()->put('number', $request->number);
        $request->session()->put('zipcode', $request->zipcode);
        $request->session()->put('city', $request->city);
        $request->session()->put('currentStep', 2);

        $this->validateRequest($request);

        return redirect('/customer/paymentInfo');
    }

    public function validateRequest(Request $request)
    {
        $request->validate([
            'street' => 'required',
            'number' => 'required|numeric',
            'zipcode' => 'required|numeric',
            'city' => 'required',
        ]);
    }
}
