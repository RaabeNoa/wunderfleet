<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegistrationController
{
    const FIRST_STEP = 1;
    const SECOND_STEP = 2;
    const THIRD_STEP = 3;

    public function redirect(Request $request)
    {
        $currentStep = $request->session()->get('currentStep') ?? null;

        switch ($currentStep){
            case self::FIRST_STEP:
                return redirect('/customer/customerInfo');
            case self::SECOND_STEP:
                return redirect('/customer/addressInfo');
            case self::THIRD_STEP:
                return redirect('/customer/paymentInfo');
            default:
                return view('welcome');
        }
    }

    public function success(Request $request)
    {
        $message = $request->session()->get('message');
        $request->session()->flush();
        return view('registration/success', compact('message', 'message'));
    }

    public function error(Request $request)
    {
        $error_message = $request->session()->get('error_message');
        $request->session()->flush();
        return view('registration/error', compact('error_message', 'error_message'));
    }
}
