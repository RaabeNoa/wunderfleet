<?php

namespace App\Http\Controllers;

use App\External\DemoPaymentService;
use App\Models\Address;
use App\Models\Customer;
use App\Models\Payment;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class CustomerController extends Controller
{
    /**
     * @var DemoPaymentService
     */
    public $demoPaymentService;

    /**
     * CustomerController constructor.
     * @param DemoPaymentService $demoPaymentService
     */
    public function __construct(DemoPaymentService $demoPaymentService)
    {
        $this->demoPaymentService = $demoPaymentService;
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $request->session()->put('currentStep', 1);
        return view('registration/customerInfo');
    }


    /**
     * @param Request $request
     * @return Application|Factory|View
     * @throws GuzzleException
     */
    public function store(Request $request)
    {
        $request->session()->put('currentStep', 3);
        try {
            $customer = $this->create($request);
            $address = $this->addCustomerAddress($request, $customer);
            $payment = $this->addCustomerPayment($request, $customer);

            $externalPaymentResult = json_decode($this->demoPaymentService->getExternalPaymentData($payment)
                ->getBody()
                ->getContents());
            $paymentDataId = $externalPaymentResult->paymentDataId;

            if(isset($paymentDataId)){
                $payment->payment_data_id = $paymentDataId;
                $this->saveAll($customer, $address, $payment);
                $request->session()
                    ->flash('message', "Registration Completed! Payment Data Id: {$paymentDataId}");
                return redirect('/registration/success');
            }

            $request->session()->flash('error_message', "Error during registration. Please try again later.");
            return redirect('/registration/error');
        }catch (\Exception $exception){
            $request->session()->flash('error_message', "Error during registration. Please try again later.");
            return redirect('/registration/error');
        }
    }

    /**
     * @param Request $request
     * @return Application|RedirectResponse|Redirector
     */
    public function setDataToSession(Request $request)
    {
        $request->session()->put('firstname', $request->firstname);
        $request->session()->put('lastname', $request->lastname);
        $request->session()->put('telephone', $request->telephone);

        $this->validateRequest($request);

        return redirect('/customer/addressInfo');
    }

    public function validateRequest(Request $request)
    {
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'telephone' => 'required|numeric'
        ]);
    }

    public function create(Request $request)
    {
        return Customer::create([
            'firstname' => $request->session()->get('firstname'),
            'lastname' => $request->session()->get('lastname'),
            'telephone' => $request->session()->get('telephone'),
        ]);
    }

    public function saveAll(Customer $customer, Address $address, Payment $payment)
    {
        $customer->save();
        $address->save();
        $payment->save();
    }

    /**
     * @param Request $request
     * @param Customer $customer
     * @return Model
     */
    public function addCustomerAddress(Request $request, Customer $customer): Address
    {
        return $customer->address()->create([
            'street' => $request->session()->get('street'),
            'number' =>$request->session()->get('number'),
            'zipcode' => $request->session()->get('zipcode'),
            'city' => $request->session()->get('city')
        ]);
    }

    /**
     * @param Request $request
     * @param Customer $customer
     * @return Model
     */
    public function addCustomerPayment(Request $request, Customer $customer): Payment
    {
        return $customer->payment()->create([
            'iban' => $request->iban,
            'account_owner' => $request->accountOwner
        ]);
    }
}
