<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PaymentController extends Controller
{

    public function index(Request $request)
    {
        $request->session()->put('currentStep', 3);
        return view('registration/paymentInfo');
    }
}
