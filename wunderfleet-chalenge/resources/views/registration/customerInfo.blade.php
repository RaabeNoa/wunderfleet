@extends('layout')

@section('header')
    Registration
@endsection
@section('header-secondary')
    - Personal Info
@endsection


@section('form-content')
    <form method="POST">
        @csrf
        <div class="row mb-3">
            <label for="firstName" class="col-sm-2 col-form-label">First Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="firstName" name="firstname">
            </div>
        </div>
        <div class="row mb-3">
            <label for="lastName" class="col-sm-2 col-form-label">Last Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="lastName" name="lastname">
            </div>
        </div>
        <div class="row mb-3">
            <label for="telephone" class="col-sm-2 col-form-label">Telephone</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="telephone" name="telephone">
            </div>
        </div>
        <button class="btn btn-dark mb2 mt-2">Next</button>
    </form>
@endsection
