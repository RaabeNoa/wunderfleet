@extends('layout')

@section('header')
<div class="alert alert-success">
    {{ $message }}
</div>
@endsection
