@extends('layout')

@section('header')
    Registration
@endsection
@section('header-secondary')
    - Address Info
@endsection


@section('form-content')
    <form method="POST" class="row g-3">
        @csrf
        <div class="col-12">
            <label for="inputStreet" class="form-label">Street</label>
            <input type="text" class="form-control" id="inputStreet" name="street" placeholder="1234 Main St">
        </div>
        <div class="col-2">
            <label for="inputHouseNumber" class="form-label">Number</label>
            <input type="number" class="form-control" name="number" id="inputHouseNumber">
        </div>
        <div class="col-md-4">
            <label for="inputZipCode" class="form-label">Zip</label>
            <input type="number" class="form-control" name="zipcode" id="inputZipCode">
        </div>
        <div class="col-md-6">
            <label for="inputCity" class="form-label">City</label>
            <input type="text" class="form-control" name="city" id="inputCity">
        </div>
        <button class="btn btn-dark mb2 mt-2">Next</button>
    </form>
@endsection
