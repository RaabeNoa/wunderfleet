@extends('layout')

@section('header')
    Registration
@endsection
@section('header-secondary')
    - Payment Info
@endsection


@section('form-content')
    <form method="post" class="row g-3">
        @csrf
        <div class="col-6">
            <label for="inputAccountOwner" class="form-label">Account Owner</label>
            <input type="text" class="form-control" placeholder="Marta Kauffman" name="accountOwner" id="inputAccountOwner">
        </div>
        <div class="col-6">
            <label for="inputIBAN" class="form-label">IBAN</label>
            <input type="text" class="form-control" placeholder="123456" name="iban" id="inputIBAN">
        </div>
        <button class="btn btn-dark mb2 mt-2">Next</button>
    </form>
@endsection
