@extends('layout')

@section('header')
    <div class="alert alert-danger">
        {{ $error_message }}
    </div>
@endsection
