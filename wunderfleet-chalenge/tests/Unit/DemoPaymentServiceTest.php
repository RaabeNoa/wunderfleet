<?php


namespace Tests\Unit;


use App\External\DemoPaymentService;
use App\Models\Payment;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Tests\TestCase;

class DemoPaymentServiceTest extends TestCase
{
    /**
     * @var DemoPaymentService
     */
    private $demoPaymentService;
    public function setUp(): void
    {
        parent::setUp();
        $client = new Client();
        $this->demoPaymentService = new DemoPaymentService($client);
    }

    /** @test */
    public function send_external_request_and_receive_response_200()
    {
        $paymentData = new Payment([
            'customer_id' => 1,
            'iban' => 'DE8234',
            'account_owner' => 'Max Mustermann'
        ]);

        $response = $this->demoPaymentService->getExternalPaymentData($paymentData);

        self::assertEquals(200, $response->getStatusCode());
    }

    /** @test */
    public function send_external_request_and_missing_params_and_throw_client_exception()
    {
        $paymentData = new Payment([
            'iban' => 'DE8234',
            'account_owner' => 'Max Mustermann'
        ]);

        $this->expectException(ClientException::class);
        $this->demoPaymentService->getExternalPaymentData($paymentData);


    }
}
